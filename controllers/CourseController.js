const Course = require('../models/Course')

module.exports.addCourse = (data) => {
	if(data.isAdmin){
		let new_course = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price,
		})

		return new_course.save().then((new_course, error) => {
			if(error){
				return false
			}

			return {
				message: 'New course successfully created!'
			}
		})
	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to access this.'
	})

	return message.then((value) => {
		return value
	})
}
	// Other way
	// return Promise.resolve('User must be ADMIN to access this.')
// }
/*module.exports.addCourse = (data) => {
    let new_course = new Course({
        name: data.course.name,
        description: data.course.description,
        price: data.course.price
    })

    return new_course.save().then((new_course, error) => {
        if(error){
            return false
        }
        return {
            message: 'New course successfully created'
        }
    })
}*/

// Get all courses
module.exports.getAllCourses = (data) => {
	return Course.find({}).then((result) => {
		return result
	})
}

// Get all ACTIVE courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then((result) => {
		return result
	})
}

// Get single course
module.exports.getCourse = (course_id) => {
	return Course.findById(course_id).then((result) => {
		return result
	})
}

// Update single course
module.exports.updateCourse = (course_id, new_data) => {
	return Course.findByIdAndUpdate(course_id, {
		name: new_data.name,
		description: new_data.description,
		price: new_data.price
	}).then((updated_course, error) => {
		if(error){
			return false
		}

		return {
			message: 'Course has been updated successfully!'
		}
	})
}

// Mini Activity
// Archive a course
module.exports.archiveCourse = (course_id) => {
	return Course.findByIdAndUpdate(course_id, {
		isActive: false
	}).then((archived_course, error) => {
		if(error){
			return falses
		}

		return {
			message: 'Course has been archived successfully!'
		}
	})
}